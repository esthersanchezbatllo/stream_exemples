/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.streamexemples;

/**
 *
 * @author EstherSanchez
 */
public class Factura {
    private int preu; 
    private String concepte;

    public Factura(String concepte, int preu) {
        this.preu = preu;
        this.concepte = concepte;
    }
    
     public int getPreu() {
        return preu;
    }

    public void setPreu(int preu) {
        this.preu = preu;
    }

     public String getConcepte() {
        return concepte;
    }
     
    public void setConcepte(String concepte) {
        this.concepte = concepte;
    }
}
