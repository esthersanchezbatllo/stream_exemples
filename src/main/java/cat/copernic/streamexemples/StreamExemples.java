/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package cat.copernic.streamexemples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author EstherSanchez
 */
public class StreamExemples {

    public static void main(String[] args) {
        /*Exemple 1: imprimeix tots els Strings de la llista*/
        List<String> llista = Arrays.asList("hola", "que", "tal");
        llista.stream().forEach((x) -> System.out.println(x));

        /*Exemple 2: imprimeix tots els strings de la llista en majúscules*/
        List< String> llista2 = Arrays.asList("hola", "que", "tal");
        //la funció map transforma elements.
        llista2.stream().map((x) -> x.toUpperCase()).forEach((x) -> System.out.println(x));

        /*Exemple 3: imprimeix tots els dnis que comencen per 2 amb el mètode filter*/
        List<String> dnis = new ArrayList<>(Arrays.asList("38044133V", "85083775J", "22936827P", "24160616J"));
        List<String> extraccio1 = dnis.stream().filter(x -> x.startsWith("2")).toList();
        extraccio1.stream().forEach((x) -> System.out.println(x));

        /*Exemple 4: */
        Factura f = new Factura("ordenador", 1000);
        Factura f2 = new Factura("movil", 300);
        Factura f3 = new Factura("impresora", 200);
        Factura f4 = new Factura("imac", 1500);
        List<Factura> lista = new ArrayList<>();
        lista.add(f);
        lista.add(f2);
        lista.add(f3);
        lista.add(f4);
        Factura facturaFiltro = lista.stream()
                .filter(x -> x.getPreu() == 300)
                .findFirst()
                .get();
        System.out.println("Concepte: " + facturaFiltro.getConcepte() + " Preu: " + facturaFiltro.getPreu());

       
    }
}
